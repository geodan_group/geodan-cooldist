const express = require('express');
const orderController = require('../../controllers/order.controller');

const router = express.Router();

router
  .route('/')
  .get(orderController.getDailyOrdersByDistributor)
  .post(orderController.resolveOrderDistributor);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Orders
 *   description: Order management
 */

/**
 * @swagger
 * /orders:
 *   post:
 *     summary: Resolve distributor for orders
 *     description: Simulates a schedule task were orders should know their distributor on created event.
 *     tags: [Orders]
 *     parameters:
*       - in: query
*         name: quantity
*         type: integer
*         required: false
*         default: 1
*         description: Quantity of orders to resolve their distributor.
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Order'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *
 *   get:
 *     summary: Get daily workload
 *     description:
 *     tags: [Orders]
 *     parameters:
 *       - in: query
 *         name: refid
 *         schema:
 *           type: number
 *         description: Distributor Id [1, 2, 3, 4].
 *       - in: query
 *         name: date
 *         schema:
 *           type: string
 *           format: date
 *         description: Working day [20-07-2023]
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of users
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Order'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 */
