const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const distributorSchema = mongoose.Schema(
  {
    refid: {
      type: Number,
      required: true
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    location: {
      type: Object,
      default: null,
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    }
  },
  {
    timestamps: true,
    collection: 'distributor'
  }
);

// add plugin that converts mongoose to json
distributorSchema.plugin(toJSON);
distributorSchema.plugin(paginate);
distributorSchema.index({ location: "2dsphere" });

/**
 * Check if distributor is still in a valid date
 * @returns {Promise<boolean>}
 */
distributorSchema.methods.isValid = async function () {
  // Distributor created date plus 1 year ahead
  var createdDate = new Date(new Date(this.createdAt).setHours(23, 59, 59, 999));
  createdDate.setFullYear(createdDate.getFullYear() + 1);

  // Current date
  var currentDate = new Date(new Date().setHours(23, 59, 59, 999));

  // Condition includes the end of the day
  return createdDate >= currentDate;
};

/**
 * @typedef Distributor
 */
const Distributor = mongoose.model('Distributor', distributorSchema);

module.exports = Distributor;
