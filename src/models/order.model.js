const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const orderSchema = mongoose.Schema(
  {
    firstname: {
      type: String,
      required: true,
      trim: true,
    },
    lastname: {
      type: String,
      required: true,
      trim: true,
    },
    zipcode: {
      type: String,
      required: true,
      trim: true,
    },
    housenumber: {
      type: Number,
      required: true
    },
    parceltype: {
      type: String,
      required: false
    },
    deliverydate: {
      type: String,
      required: true
    },  
    location: {
      type: Object,
      default: null,
    },  
    distributor: {
      type: Object,
      default: null,
    },
    settled: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true,
    collection: 'order'
  }
);

// add plugin that converts mongoose to json
orderSchema.plugin(toJSON);
orderSchema.plugin(paginate);

/**
 * Check if distributor has already been assigned to order
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
orderSchema.statics.doDistributorBeenAssigned = async function () {
  const order = this;
  return order.location && order.distributor && order.settled;
};

/**
 * @typedef Order
 */
const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
