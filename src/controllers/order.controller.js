const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { orderService } = require('../services');
const { Distributor } = require('../models');

const getDailyOrdersByDistributor = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['refid', 'date']);
  const options = pick(req.query, ['limit', 'page']);
  const result = await orderService.getDailyOrdersByDistributor(filter, options);
  res.send(result);
});

const resolveOrderDistributor = catchAsync(async (req, res) => {
  const options = pick(req.query, ['quantity']);
  const orders = await orderService.findOrdersDistributors(options);
  res.send(orders);
});

module.exports = {
  getDailyOrdersByDistributor,
  resolveOrderDistributor
};
