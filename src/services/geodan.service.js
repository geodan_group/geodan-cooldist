const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const fetch = require('node-fetch');

/**
 * Fetch geodan map services
 * @param {Object} params - Search params
 * @param {string} [params.postalCode] - Postal code
 * @param {number} [params.houseNumber] - Building number
 * @returns {Object}
 */
const searchLocation = async (params) => {
  const type = "address";
  const country = "NLD";
  const postalCode = params.postalCode;
  const building = "housenumber";
  const houseNumber = params.houseNumber;
  const forceResult = true;
  const limit = 10;
  const offset = 0;
  const servicekey = "4a8a51d9-aee3-11e9-8aac-005056805b87";
  let data = {};
  try {
    const response = await fetch([`https://services.geodan.nl/geosearch/v2/search`,
      `?type=${type}`,
      `&country=${country}`,
      `&postalCode=${postalCode}`,
      `&building=${building}`,
      `&houseNumber=${houseNumber}`,
      `&forceResult=${forceResult}`,
      `&limit=${limit}`,
      `&offset=${offset}`,
      `&servicekey=${servicekey}`].join('')
    );
    data = await response.json();
  } catch (error) {
    console.error(error);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Something went wrong, try again.');
  }
  return data;
};

module.exports = {
  searchLocation
};
