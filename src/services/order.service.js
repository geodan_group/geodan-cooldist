const httpStatus = require('http-status');
const pick = require('../utils/pick');
const { Order, Distributor } = require('../models');
const ApiError = require('../utils/ApiError');
const geodanService = require('../services/geodan.service');

/**
 * Get all orders for a given distributor and date
 * @param {Object} filter - Mongo filter
 * @param {number} [filter.refid] - Distributor reference id
 * @param {string} [filter.date] - Order delivery date
 * @param {Object} options - Query options
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getDailyOrdersByDistributor = async (filter, options) => {
  const orders = await Order.paginateDailyWorkload(filter, options);
  return orders;
};

/**
 * Get order by id
 * @param {ObjectId} id
 * @returns {Promise<Order>}
 */
const getOrderById = async (id) => {
  return Order.findById(id);
};

/**
 * Update order
 * @param {Object} order
 * @returns {Promise<Order>}
 */
const updateOrder = async (order) => {
  // TODO - Validations
  await order.save();
  return order;
};

/**
 * Resolve next n order Geo data and distributor
 * @param {object} options
 * @param {number} [options.quantity] Quantity of orders to attend (default = 1)
 * @returns {Promise<Order>}
 */
const findOrdersDistributors = async (options) => {
  // Querying for orders without location and distributor, wich still hasn't been settled
  const orders = await Order.find({$or: [{settled: {$exists: false}}, {settled: false}]}).limit(+options.quantity || 1);
  if (!orders) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Orders not found');
  }

  return await Promise.all( orders
  
  // Process each order
  .map(async (order) => {

    // Fetch Geo data from GeodanMaps Service to each order delivery address
    const geoData = await geodanService.searchLocation({
      postalCode: order.zipcode,
      houseNumber: order.housenumber
    });

    // If succeed
    if(
        geoData && 
        geoData.features && 
        geoData.features[0] && 
        geoData.features[0].centroid && 
        geoData.features[0].centroid.coordinates
      ) {

        // Extract coordinates from Geo data
        const coordinates = [
          +geoData.features[0].centroid.coordinates[0], /*Longitude*/
          +geoData.features[0].centroid.coordinates[1]  /*Latitude*/
        ];

        // If valid coordinates
        if(
            (coordinates[0] >= -180 && coordinates[0] <= 180 /*Longitude*/) &&
            (coordinates[1] >= -90 && coordinates[0] <= 90 /*Latitude*/)
          ) {

            // Use Geo data coordinates to find the nearest distributor (as the crow flyes), using $near operator from Mongodb
            const distributor = await getOrderDistributionLocation(coordinates);
            
            // Settling Geo data and distributor to the order
            Object.assign(order, {
                settled: true,           // Indicates that the order is ready to deliver
                location: geoData,       // Geo data
                distributor: distributor // Nearest distributor
              });

            await updateOrder(order);
          }
    }

    return order;
  }))
  
  // Filter only settled orders
  .then(orders => orders.filter(order => order.settled));
};

/**
 * Resolve order distribution location
 * @param {array} [longitude, latitude] Coordinates
 * @returns {Object}
 */
const getOrderDistributionLocation = async (coordinates) => {
  // Find all distributors sorted from nearest to farthest
  const distributors = await Distributor.find({ 
      location: { 
          $near: { 
              $geometry: { 
                  type: "Point", 
                  coordinates: [coordinates[0], coordinates[1]],
                  $maxDistance : 1
              } 
          } 
      }
  });

  return await Promise.all(  distributors

    // Mapping available distributors, no more than 1 year of created
    .map(async distributor => await distributor.isValid() && distributor)
  )

  // Filter only valid distributors
  .then(distributors => distributors.find(distributor => distributor));
};

module.exports = {
  getDailyOrdersByDistributor,
  findOrdersDistributors
};
