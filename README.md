# RESTful API Node Server

Based on a boilerplate/starter project for quickly building RESTful APIs using Node.js, Express, and Mongoose.

## Quick Start

(1) Install the dependencies:

```bash
npm install
```

(2) Running locally:

```bash
npm run dev
```

(3) Listening on:

[Node-App] - localhost:3000 \
[Swagger]  - localhost:3000/v1/docs \
[MongoDB]  - mongodb+srv://avazquez0191:SYGK2TWH24T5i6gv@geodan-c-1.0c2lyno.mongodb.net/geodan-cooldist

(4) How to:

[Access-API] - From Swagger or Postman Collection (included in project) \
[Access-DB]  - Using MongoDB string connector, also a script is included in the project './db/geodan_cooldist_db.js'


## Table of Contents

- [Commands](#commands)
- [Environment Variables](#environment-variables)
- [Project Structure](#project-structure)
- [API Documentation](#api-documentation)

## Commands

Running locally:

```bash
npm run dev
```

Running in production:

```bash
npm run start
```

Testing:

```bash
# run all tests
npm run test

# run all tests in watch mode
npm run test:watch

# run test coverage
npm run coverage
```

Linting:

```bash
# run ESLint
npm run lint

# fix ESLint errors
npm run lint:fix

# run prettier
npm run prettier

# fix prettier errors
npm run prettier:fix
```

## Environment Variables

The environment variables can be found and modified in the `.env` file. They come with these default values:

## Project Structure

```
src\
 |--config\         # Environment variables and configuration related things
 |--controllers\    # Route controllers (controller layer)
 |--docs\           # Swagger files
 |--middlewares\    # Custom express middlewares
 |--models\         # Mongoose models (data layer)
 |--routes\         # Routes
 |--services\       # Business logic (service layer)
 |--utils\          # Utility classes and functions
 |--validations\    # Request data validation schemas
 |--app.js          # Express app
 |--index.js        # App entry point
```

### API Endpoints

List of available routes:

**Order routes**:\
`GET /v1/orders` - get daily workload\
`POST /v1/orders` - resolve distributor
