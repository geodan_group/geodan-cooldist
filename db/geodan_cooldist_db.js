db.getCollection("distributor").insertMany([
{
    "refid": 1,
    "name": "Location Amsterdam", 
    "location": {
        "type": "Point",
        "coordinates": [4.910305, 52.342346]
    },
    "createdAt": "2023-07-14",
    "updatedAt": "2023-07-14"
},
{
    "refid": 2,
    "name": "Location den Bosch", 
    "location": {
        "type": "Point",
        "coordinates": [5.299683, 51.69174]
    },
    "createdAt": "2021-07-14",
    "updatedAt": "2023-07-14"
},
{
    "refid": 3,
    "name": "Location Utrecht", 
    "location": {
        "type": "Point",
        "coordinates": [5.104480, 52.092876]
    },
    "createdAt": "2022-07-14",
    "updatedAt": "2022-07-14"
},
{
    "refid": 4,
    "name": "Location Den Helder", 
    "location": {
        "type": "Point",
        "coordinates": [4.75933, 52.95988]
    },
    "createdAt": "2021-07-14",
    "updatedAt": "2021-07-14"
}
]);

db.getCollection("distributor").createIndex({"location":"2dsphere"});

db.getCollection("order").insertMany([
  {
    "firstname": "Li",
    "lastname": "Mullins",
    "zipcode": "4815PG",
    "housenumber": 18,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023",
    "location": {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "id": "TkxELy8vL2FkZHJlc3MtMDc1ODEwMDAwMDAwNTY5Ni0wNzU4MjAwMDAwMDI4NjE4",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        4.7362788,
                        51.599484499999996
                    ]
                },
                "properties": {
                    "area": 259.0,
                    "country": "The Netherlands",
                    "naturalId": "0758200000028618",
                    "exactMatch": false,
                    "city": "Breda",
                    "constructionYear": 2005,
                    "purpose": "woonfunctie",
                    "addressType": "v",
                    "postalCode": "4815PG",
                    "precision": "address",
                    "municipality": "Breda",
                    "houseNumber": 18,
                    "type": "address",
                    "relatedId": "0758010000004968",
                    "buildingId": "0758100000005696",
                    "street": "Groot Hoogsteen",
                    "name": "Groot Hoogsteen 18, Breda",
                    "state": "Noord-Brabant",
                    "primaryAddress": true,
                    "buildingStatus": "Verblijfsobject in gebruik"
                },
                "centroid": {
                    "type": "Point",
                    "coordinates": [
                        4.7362788,
                        51.599484499999996
                    ]
                }
            }
        ],
        "numberMatched": 1,
        "numberReturned": 1
    },
    "distributor": {
        "refid": 2,
        "name": "Location den Bosch", 
        "location": {
            "type": "Point",
            "coordinates": [5.299683, 51.69174]
        }
    },
    "settled": true
  },
  {
    "firstname": "Salma",
    "lastname": "Underwood",
    "zipcode": "7581DB",
    "housenumber": 25,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023",
    "settled": false
  },
  {
    "firstname": "Badreddine",
    "lastname": "Phelps",
    "zipcode": "6466RW",
    "housenumber": 37,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chanine",
    "lastname": "Perkins",
    "zipcode": "7391DA",
    "housenumber": 51,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Glen",
    "lastname": "Guerrero",
    "zipcode": "2023BJ",
    "housenumber": 293,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sanaz",
    "lastname": "Collins",
    "zipcode": "3034EA",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bonnie",
    "lastname": "Allen",
    "zipcode": "5628ZA",
    "housenumber": 37,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Connie",
    "lastname": "Calderon",
    "zipcode": "3131HT",
    "housenumber": 14,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nelson",
    "lastname": "Graves",
    "zipcode": "6164JK",
    "housenumber": 84,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Damyan",
    "lastname": "Navarro",
    "zipcode": "1221SB",
    "housenumber": 74,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Juultje",
    "lastname": "Carrillo",
    "zipcode": "9534PA",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Diena",
    "lastname": "Chavez",
    "zipcode": "2523JP",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chaira",
    "lastname": "Hussain",
    "zipcode": "4001XC",
    "housenumber": 8,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Merith",
    "lastname": "Aguilar",
    "zipcode": "5234GD",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jochen",
    "lastname": "Thomas",
    "zipcode": "8731DW",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Styn",
    "lastname": "Mccann",
    "zipcode": "6093CR",
    "housenumber": 8,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Inès",
    "lastname": "Knight",
    "zipcode": "6135JV",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ismaïl",
    "lastname": "Garza",
    "zipcode": "1616RL",
    "housenumber": 4,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Prisilla",
    "lastname": "Curry",
    "zipcode": "3925EW",
    "housenumber": 41,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alwin",
    "lastname": "Vaughn",
    "zipcode": "8937BK",
    "housenumber": 85,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Noor",
    "lastname": "Richardson",
    "zipcode": "6217EW",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dwayn",
    "lastname": "Heath",
    "zipcode": "1082EB",
    "housenumber": 125,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Myrle",
    "lastname": "Burke",
    "zipcode": "6211HD",
    "housenumber": 4,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Arnold",
    "lastname": "Fleming",
    "zipcode": "1352GR",
    "housenumber": 183,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nermin",
    "lastname": "Powell",
    "zipcode": "1931AR",
    "housenumber": 34,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Thys",
    "lastname": "Lewis",
    "zipcode": "6074AH",
    "housenumber": 4,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jason",
    "lastname": "Hines",
    "zipcode": "3451JD",
    "housenumber": 44,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Geurtje",
    "lastname": "Reyes",
    "zipcode": "4251HJ",
    "housenumber": 44,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Othman",
    "lastname": "Poole",
    "zipcode": "8219PT",
    "housenumber": 53,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hajar",
    "lastname": "Meyer",
    "zipcode": "1432JN",
    "housenumber": 24,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Attila",
    "lastname": "Dawson",
    "zipcode": "4872SB",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jent",
    "lastname": "Jordan",
    "zipcode": "9744GK",
    "housenumber": 76,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jori",
    "lastname": "Norman",
    "zipcode": "9743EK",
    "housenumber": 165,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sertan",
    "lastname": "Sherman",
    "zipcode": "1054PP",
    "housenumber": 53,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jacko",
    "lastname": "Patel",
    "zipcode": "3071ZH",
    "housenumber": 76,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sunita",
    "lastname": "Mejia",
    "zipcode": "1013DW",
    "housenumber": 304,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Manna",
    "lastname": "Gross",
    "zipcode": "9761AB",
    "housenumber": 20,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Juup",
    "lastname": "Richards",
    "zipcode": "6466PA",
    "housenumber": 25,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Janouk",
    "lastname": "Burton",
    "zipcode": "8936AT",
    "housenumber": 16,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cedric",
    "lastname": "Christensen",
    "zipcode": "7051AL",
    "housenumber": 4,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Willian",
    "lastname": "Mills",
    "zipcode": "1945TG",
    "housenumber": 229,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Serhat",
    "lastname": "Malone",
    "zipcode": "7471ML",
    "housenumber": 22,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chiel",
    "lastname": "Graham",
    "zipcode": "6471CJ",
    "housenumber": 2,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fenno",
    "lastname": "Luna",
    "zipcode": "2723BA",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jemima",
    "lastname": "King",
    "zipcode": "1399KM",
    "housenumber": 2,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Liselot",
    "lastname": "Naylor",
    "zipcode": "1934PA",
    "housenumber": 29,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sude",
    "lastname": "Casey",
    "zipcode": "4904DC",
    "housenumber": 6,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jorgen",
    "lastname": "Taylor",
    "zipcode": "1613GW",
    "housenumber": 312,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nathanja",
    "lastname": "Tucker",
    "zipcode": "7701CJ",
    "housenumber": 148,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chenna",
    "lastname": "Jenkins",
    "zipcode": "3245XA",
    "housenumber": 21,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bedia",
    "lastname": "Johnson",
    "zipcode": "9502BP",
    "housenumber": 5,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Semiha",
    "lastname": "Berry",
    "zipcode": "8491CL",
    "housenumber": 26,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Miryam",
    "lastname": "Gutierrez",
    "zipcode": "3353GB",
    "housenumber": 30,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yaron",
    "lastname": "Buchanan",
    "zipcode": "7335PR",
    "housenumber": 106,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Badr",
    "lastname": "Gibson",
    "zipcode": "6049LS",
    "housenumber": 56,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Florian",
    "lastname": "Carpenter",
    "zipcode": "6081AH",
    "housenumber": 5,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Katia",
    "lastname": "Fields",
    "zipcode": "2612AT",
    "housenumber": 15,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Evianne",
    "lastname": "Stewart",
    "zipcode": "1687CG",
    "housenumber": 20,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wilhelmina",
    "lastname": "Lord",
    "zipcode": "5045XR",
    "housenumber": 20,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Corianne",
    "lastname": "Willis",
    "zipcode": "1086VE",
    "housenumber": 83,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Violette",
    "lastname": "Dale",
    "zipcode": "3825XD",
    "housenumber": 45,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cherrelle",
    "lastname": "Barton",
    "zipcode": "7608VL",
    "housenumber": 41,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jelisa",
    "lastname": "Castaneda",
    "zipcode": "1223EK",
    "housenumber": 35,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Anniek",
    "lastname": "Daniel",
    "zipcode": "2042ER",
    "housenumber": 145,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dursun",
    "lastname": "Wise",
    "zipcode": "7826EE",
    "housenumber": 33,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hatice",
    "lastname": "Bell",
    "zipcode": "6533MG",
    "housenumber": 109,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Amina",
    "lastname": "French",
    "zipcode": "6707CA",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Roeline",
    "lastname": "Stephens",
    "zipcode": "3039HA",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Maia",
    "lastname": "Wyatt",
    "zipcode": "1022LC",
    "housenumber": 264,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hieke",
    "lastname": "Byrne",
    "zipcode": "3312PN",
    "housenumber": 37,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Josua",
    "lastname": "Austin",
    "zipcode": "9982AM",
    "housenumber": 268,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jacey",
    "lastname": "Ward",
    "zipcode": "6657BH",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rebekka",
    "lastname": "Mac",
    "zipcode": "8162WT",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Birte",
    "lastname": "Wood",
    "zipcode": "5341JA",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marnix",
    "lastname": "Burns",
    "zipcode": "4571MK",
    "housenumber": 23,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Neriman",
    "lastname": "Schwartz",
    "zipcode": "2312CH",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Numan",
    "lastname": "Carr",
    "zipcode": "5035DB",
    "housenumber": 12,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Blessing",
    "lastname": "Moody",
    "zipcode": "6333DE",
    "housenumber": 42,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Umar",
    "lastname": "Montgomery",
    "zipcode": "1422AC",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dünya",
    "lastname": "Barker",
    "zipcode": "8321HZ",
    "housenumber": 24,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Banu",
    "lastname": "Schultz",
    "zipcode": "9665ET",
    "housenumber": 15,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ngoc",
    "lastname": "Green",
    "zipcode": "2343NZ",
    "housenumber": 24,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tristan",
    "lastname": "Todd",
    "zipcode": "1273WJ",
    "housenumber": 54,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alex",
    "lastname": "Brooks",
    "zipcode": "7475BX",
    "housenumber": 43,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jurrien",
    "lastname": "Fernandez",
    "zipcode": "1106BV",
    "housenumber": 62,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Delia",
    "lastname": "Parker",
    "zipcode": "5074PP",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wendeline",
    "lastname": "Alexander",
    "zipcode": "3526TV",
    "housenumber": 284,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Katarzyna",
    "lastname": "Pacheco",
    "zipcode": "6823KT",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Davinia",
    "lastname": "Schneider",
    "zipcode": "3474KB",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ad",
    "lastname": "Reid",
    "zipcode": "5831JV",
    "housenumber": 68,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Silvano",
    "lastname": "Larson",
    "zipcode": "6545BA",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gabi",
    "lastname": "Hart",
    "zipcode": "1221HA",
    "housenumber": 13,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rajshri",
    "lastname": "Mccarthy",
    "zipcode": "3076TK",
    "housenumber": 31,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gidion",
    "lastname": "Doyle",
    "zipcode": "5421DN",
    "housenumber": 65,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jenno",
    "lastname": "Wright",
    "zipcode": "1186CH",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lydie",
    "lastname": "Bradley",
    "zipcode": "1487ME",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jamiro",
    "lastname": "Hughes",
    "zipcode": "1063KW",
    "housenumber": 208,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Suzana",
    "lastname": "Gough",
    "zipcode": "9679CG",
    "housenumber": 92,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dana",
    "lastname": "Cassidy",
    "zipcode": "2172AB",
    "housenumber": 24,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Riekje",
    "lastname": "Carpenter",
    "zipcode": "7165AD",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Laura",
    "lastname": "Farmer",
    "zipcode": "7475MT",
    "housenumber": 28,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Michélle",
    "lastname": "Leon",
    "zipcode": "7391DH",
    "housenumber": 21,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Anouck",
    "lastname": "Chavez",
    "zipcode": "4001EN",
    "housenumber": 136,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alien",
    "lastname": "Wood",
    "zipcode": "3571NN",
    "housenumber": 50,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Michel",
    "lastname": "Santiago",
    "zipcode": "4691LT",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Aidan",
    "lastname": "Alexander",
    "zipcode": "2561AT",
    "housenumber": 186,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lieselotte",
    "lastname": "Payne",
    "zipcode": "4651WV",
    "housenumber": 14,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kalle",
    "lastname": "Graham",
    "zipcode": "1326DZ",
    "housenumber": 21,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Robert",
    "lastname": "Robinson",
    "zipcode": "4462TB",
    "housenumber": 49,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fjodor",
    "lastname": "Rice",
    "zipcode": "4194RG",
    "housenumber": 43,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lucy",
    "lastname": "Li",
    "zipcode": "1619BZ",
    "housenumber": 18,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yorik",
    "lastname": "Hamilton",
    "zipcode": "4744AT",
    "housenumber": 31,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Geoffry",
    "lastname": "Fraser",
    "zipcode": "1422GX",
    "housenumber": 8,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Norbertus",
    "lastname": "Stewart",
    "zipcode": "9611LW",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yuna",
    "lastname": "Romero",
    "zipcode": "2631AV",
    "housenumber": 18,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Phebe",
    "lastname": "Singh",
    "zipcode": "5371DA",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alistair",
    "lastname": "Mack",
    "zipcode": "6369TZ",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Xanthe",
    "lastname": "Zimmerman",
    "zipcode": "6163BL",
    "housenumber": 62,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Durmuş",
    "lastname": "Simmons",
    "zipcode": "1013BD",
    "housenumber": 214,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Charisa",
    "lastname": "Hale",
    "zipcode": "5331KD",
    "housenumber": 22,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Abigaïl",
    "lastname": "Little",
    "zipcode": "2241CL",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Daoud",
    "lastname": "Mcdaniel",
    "zipcode": "6151GX",
    "housenumber": 21,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Stèphanie",
    "lastname": "Day",
    "zipcode": "9481AL",
    "housenumber": 4,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Michaela",
    "lastname": "Ramsey",
    "zipcode": "3844PJ",
    "housenumber": 65,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jori",
    "lastname": "Garner",
    "zipcode": "6691GA",
    "housenumber": 5,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gidion",
    "lastname": "Harrison",
    "zipcode": "6074DN",
    "housenumber": 2,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Maroussia",
    "lastname": "Fernandez",
    "zipcode": "1132LN",
    "housenumber": 10,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Elanur",
    "lastname": "Strickland",
    "zipcode": "5317JB",
    "housenumber": 15,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sharina",
    "lastname": "Allison",
    "zipcode": "4617GE",
    "housenumber": 23,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ülkü",
    "lastname": "Read",
    "zipcode": "9401BX",
    "housenumber": 41,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Aniel",
    "lastname": "Gilbert",
    "zipcode": "1062JB",
    "housenumber": 421,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Catherine",
    "lastname": "Wells",
    "zipcode": "8355BZ",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rokus",
    "lastname": "Hodgson",
    "zipcode": "3741DD",
    "housenumber": 62,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Darshan",
    "lastname": "Henry",
    "zipcode": "8862XC",
    "housenumber": 75,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Brittney",
    "lastname": "Owen",
    "zipcode": "3075VP",
    "housenumber": 17,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sacha",
    "lastname": "Maxwell",
    "zipcode": "2562CT",
    "housenumber": 54,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sefa",
    "lastname": "Pratt",
    "zipcode": "1445EB",
    "housenumber": 31,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Morgan",
    "lastname": "Norman",
    "zipcode": "3136KV",
    "housenumber": 692,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gabriella",
    "lastname": "Best",
    "zipcode": "1064KB",
    "housenumber": 11,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kellie",
    "lastname": "Schwartz",
    "zipcode": "6691DX",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alican",
    "lastname": "Crawford",
    "zipcode": "4163LJ",
    "housenumber": 137,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Desney",
    "lastname": "Hubbard",
    "zipcode": "7091BJ",
    "housenumber": 16,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Liban",
    "lastname": "Gibbs",
    "zipcode": "2595EP",
    "housenumber": 123,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Morten",
    "lastname": "Wilkes",
    "zipcode": "5751GZ",
    "housenumber": 82,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hermanus",
    "lastname": "O'ConnorO'",
    "zipcode": "2012WJ",
    "housenumber": 296,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lale",
    "lastname": "Lindsey",
    "zipcode": "3221BX",
    "housenumber": 8,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ruthger",
    "lastname": "Greene",
    "zipcode": "4333AC",
    "housenumber": 106,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jur",
    "lastname": "Fisher",
    "zipcode": "2513SE",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Haci",
    "lastname": "English",
    "zipcode": "1103DR",
    "housenumber": 59,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gabriela",
    "lastname": "Serrano",
    "zipcode": "7418CK",
    "housenumber": 4,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Teus",
    "lastname": "Park",
    "zipcode": "4615JZ",
    "housenumber": 48,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ilham",
    "lastname": "Ortega",
    "zipcode": "3311VE",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Elly",
    "lastname": "Patrick",
    "zipcode": "9661AJ",
    "housenumber": 58,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mirela",
    "lastname": "Johnson",
    "zipcode": "9561SJ",
    "housenumber": 127,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Door",
    "lastname": "Aguilar",
    "zipcode": "3212LA",
    "housenumber": 67,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yen",
    "lastname": "Schultz",
    "zipcode": "3192WB",
    "housenumber": 63,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Udo",
    "lastname": "Curtis",
    "zipcode": "1011NC",
    "housenumber": 194,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Okke",
    "lastname": "Singleton",
    "zipcode": "8331PL",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Boris",
    "lastname": "Manning",
    "zipcode": "1562BM",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Shanna",
    "lastname": "Daniel",
    "zipcode": "1211AT",
    "housenumber": 96,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sarie",
    "lastname": "Logan",
    "zipcode": "2441EH",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Grietje",
    "lastname": "Woodward",
    "zipcode": "1783GV",
    "housenumber": 14,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jaydee",
    "lastname": "Wilkinson",
    "zipcode": "3906AN",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Justinus",
    "lastname": "Walton",
    "zipcode": "2651HP",
    "housenumber": 73,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cian",
    "lastname": "Lewis",
    "zipcode": "3144NE",
    "housenumber": 174,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Phil",
    "lastname": "Vazquez",
    "zipcode": "3762SP",
    "housenumber": 39,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Darja",
    "lastname": "Huff",
    "zipcode": "5622KZ",
    "housenumber": 44,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Damaris",
    "lastname": "Ramirez",
    "zipcode": "3863CA",
    "housenumber": 16,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Carissa",
    "lastname": "Thomas",
    "zipcode": "3025RN",
    "housenumber": 17,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tuba",
    "lastname": "Gill",
    "zipcode": "2037XE",
    "housenumber": 53,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jenneke",
    "lastname": "Cortez",
    "zipcode": "3195HE",
    "housenumber": 357,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Eyup",
    "lastname": "Terry",
    "zipcode": "6081DA",
    "housenumber": 16,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bibian",
    "lastname": "Riley",
    "zipcode": "2215CN",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wieneke",
    "lastname": "Powers",
    "zipcode": "1788ET",
    "housenumber": 1023,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lenie",
    "lastname": "Cole",
    "zipcode": "3079XN",
    "housenumber": 65,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jersey",
    "lastname": "Long",
    "zipcode": "1967GE",
    "housenumber": 26,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yasmine",
    "lastname": "Flores",
    "zipcode": "6717AL",
    "housenumber": 22,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Aynur",
    "lastname": "May",
    "zipcode": "1851HN",
    "housenumber": 43,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Volkan",
    "lastname": "Armstrong",
    "zipcode": "2994ED",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Annemarie",
    "lastname": "Huang",
    "zipcode": "5437DA",
    "housenumber": 4,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jerusha",
    "lastname": "Hopkins",
    "zipcode": "5575CK",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Constant",
    "lastname": "Barrett",
    "zipcode": "2377DC",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fedoua",
    "lastname": "Cannon",
    "zipcode": "3842JX",
    "housenumber": 17,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Laurie",
    "lastname": "Rose",
    "zipcode": "4251HM",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sylvian",
    "lastname": "Carr",
    "zipcode": "1033WC",
    "housenumber": 41,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Danni",
    "lastname": "Francis",
    "zipcode": "3563HX",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Whitney",
    "lastname": "Howell",
    "zipcode": "2811PZ",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Elfi",
    "lastname": "Wong",
    "zipcode": "1456CA",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cem",
    "lastname": "Pearson",
    "zipcode": "3621NC",
    "housenumber": 6,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Leandro",
    "lastname": "Butler",
    "zipcode": "7894DB",
    "housenumber": 20,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Justian",
    "lastname": "Roberson",
    "zipcode": "2586BT",
    "housenumber": 660,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sayfeddine",
    "lastname": "Mullins",
    "zipcode": "6601GR",
    "housenumber": 112,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Darnell",
    "lastname": "Underwood",
    "zipcode": "3261PT",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cyrus",
    "lastname": "Phelps",
    "zipcode": "6271BP",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Keimpe",
    "lastname": "Perkins",
    "zipcode": "3481CC",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lion",
    "lastname": "Guerrero",
    "zipcode": "6828KR",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Shiela",
    "lastname": "Collins",
    "zipcode": "1091JT",
    "housenumber": 43,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hermannus",
    "lastname": "Allen",
    "zipcode": "5113BC",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bianca",
    "lastname": "Calderon",
    "zipcode": "5473HJ",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Selenay",
    "lastname": "Graves",
    "zipcode": "1018LK",
    "housenumber": 27,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Else",
    "lastname": "Navarro",
    "zipcode": "7707DV",
    "housenumber": 24,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yeter",
    "lastname": "Carrillo",
    "zipcode": "9746AS",
    "housenumber": 38,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rüveyda",
    "lastname": "Chavez",
    "zipcode": "1018LT",
    "housenumber": 311,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Esmée",
    "lastname": "Hussain",
    "zipcode": "6015AG",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Floran",
    "lastname": "Aguilar",
    "zipcode": "5721GV",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Frens",
    "lastname": "Thomas",
    "zipcode": "3823VJ",
    "housenumber": 55,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sonam",
    "lastname": "Mccann",
    "zipcode": "9401AZ",
    "housenumber": 117,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Shaina",
    "lastname": "Knight",
    "zipcode": "1681NA",
    "housenumber": 38,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nedim",
    "lastname": "Garza",
    "zipcode": "5308KA",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chakira",
    "lastname": "Curry",
    "zipcode": "2013TE",
    "housenumber": 186,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Asena",
    "lastname": "Vaughn",
    "zipcode": "5801AL",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Janet",
    "lastname": "Richardson",
    "zipcode": "5725CL",
    "housenumber": 40,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chiméne",
    "lastname": "Heath",
    "zipcode": "2402ZG",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Irem",
    "lastname": "Burke",
    "zipcode": "2771MS",
    "housenumber": 388,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "India",
    "lastname": "Fleming",
    "zipcode": "4431RG",
    "housenumber": 44,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Eibert",
    "lastname": "Powell",
    "zipcode": "9864PH",
    "housenumber": 53,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Phylicia",
    "lastname": "Lewis",
    "zipcode": "7722LX",
    "housenumber": 40,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cyrus",
    "lastname": "Hines",
    "zipcode": "5981XT",
    "housenumber": 14,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Daniele",
    "lastname": "Reyes",
    "zipcode": "6269DZ",
    "housenumber": 69,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Louw",
    "lastname": "Poole",
    "zipcode": "5261BN",
    "housenumber": 73,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nataly",
    "lastname": "Meyer",
    "zipcode": "5237WX",
    "housenumber": 100,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tirsa",
    "lastname": "Dawson",
    "zipcode": "3992AK",
    "housenumber": 8,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Akshay",
    "lastname": "Jordan",
    "zipcode": "6883BJ",
    "housenumber": 148,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mamadou",
    "lastname": "Norman",
    "zipcode": "9743ND",
    "housenumber": 113,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alper",
    "lastname": "Sherman",
    "zipcode": "2151KR",
    "housenumber": 19,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lynda",
    "lastname": "Patel",
    "zipcode": "3072BH",
    "housenumber": 114,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Monisha",
    "lastname": "Mejia",
    "zipcode": "4513KT",
    "housenumber": 44,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Niek",
    "lastname": "Gross",
    "zipcode": "6523RW",
    "housenumber": 325,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wubbo",
    "lastname": "Richards",
    "zipcode": "6842DD",
    "housenumber": 19,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Geurt",
    "lastname": "Burton",
    "zipcode": "5066XH",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Quinn",
    "lastname": "Christensen",
    "zipcode": "6211WE",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Annely",
    "lastname": "Mills",
    "zipcode": "5464VS",
    "housenumber": 13,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Seth",
    "lastname": "Malone",
    "zipcode": "6828JL",
    "housenumber": 80,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gülizar",
    "lastname": "Graham",
    "zipcode": "2562EL",
    "housenumber": 322,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dinanda",
    "lastname": "Luna",
    "zipcode": "2163AD",
    "housenumber": 119,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lieneke",
    "lastname": "King",
    "zipcode": "2295LP",
    "housenumber": 10,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gaston",
    "lastname": "Naylor",
    "zipcode": "2585HA",
    "housenumber": 8,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yeliz",
    "lastname": "Casey",
    "zipcode": "5674AA",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Inez",
    "lastname": "Taylor",
    "zipcode": "2871JZ",
    "housenumber": 24,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Melissa",
    "lastname": "Tucker",
    "zipcode": "4613GJ",
    "housenumber": 39,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Akke",
    "lastname": "Jenkins",
    "zipcode": "6701AT",
    "housenumber": 160,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jans",
    "lastname": "Johnson",
    "zipcode": "5106NS",
    "housenumber": 6,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Masoud",
    "lastname": "Berry",
    "zipcode": "1965NZ",
    "housenumber": 29,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Judie",
    "lastname": "Gutierrez",
    "zipcode": "3813XE",
    "housenumber": 141,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Humphrey",
    "lastname": "Buchanan",
    "zipcode": "1094ST",
    "housenumber": 31,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Thierry",
    "lastname": "Gibson",
    "zipcode": "2401AD",
    "housenumber": 175,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ernst",
    "lastname": "Carpenter",
    "zipcode": "1056JW",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Valencia",
    "lastname": "Fields",
    "zipcode": "3741HC",
    "housenumber": 19,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Raissa",
    "lastname": "Stewart",
    "zipcode": "2275CG",
    "housenumber": 29,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nouri",
    "lastname": "Lord",
    "zipcode": "6466SV",
    "housenumber": 21,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dimas",
    "lastname": "Willis",
    "zipcode": "6216PJ",
    "housenumber": 36,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Huda",
    "lastname": "Dale",
    "zipcode": "4208DC",
    "housenumber": 77,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Angelique",
    "lastname": "Barton",
    "zipcode": "4141CD",
    "housenumber": 100,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jody",
    "lastname": "Castaneda",
    "zipcode": "1851MJ",
    "housenumber": 68,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hassana",
    "lastname": "Daniel",
    "zipcode": "1051KB",
    "housenumber": 62,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Musa",
    "lastname": "Wise",
    "zipcode": "2355DA",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cherelle",
    "lastname": "Bell",
    "zipcode": "1679XM",
    "housenumber": 108,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Majda",
    "lastname": "French",
    "zipcode": "2652XB",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Driesje",
    "lastname": "Stephens",
    "zipcode": "4538AP",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Juli",
    "lastname": "Wyatt",
    "zipcode": "1078NZ",
    "housenumber": 244,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Isadora",
    "lastname": "Byrne",
    "zipcode": "2671CE",
    "housenumber": 33,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Defne",
    "lastname": "Austin",
    "zipcode": "7215SC",
    "housenumber": 10,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sammie",
    "lastname": "Ward",
    "zipcode": "1121BV",
    "housenumber": 61,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Laury",
    "lastname": "Mac",
    "zipcode": "2571HR",
    "housenumber": 236,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ebenezer",
    "lastname": "Wood",
    "zipcode": "2516SE",
    "housenumber": 149,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Zohra",
    "lastname": "Burns",
    "zipcode": "1217TD",
    "housenumber": 8,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Redwan",
    "lastname": "Schwartz",
    "zipcode": "7876CB",
    "housenumber": 41,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ying",
    "lastname": "Carr",
    "zipcode": "5237WX",
    "housenumber": 80,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ichelle",
    "lastname": "Moody",
    "zipcode": "1823WB",
    "housenumber": 56,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ayden",
    "lastname": "Montgomery",
    "zipcode": "5291AJ",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Emirhan",
    "lastname": "Barker",
    "zipcode": "3032XK",
    "housenumber": 27,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alec",
    "lastname": "Schultz",
    "zipcode": "1622BX",
    "housenumber": 27,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Douha",
    "lastname": "Green",
    "zipcode": "9401GJ",
    "housenumber": 75,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Margit",
    "lastname": "Todd",
    "zipcode": "5502HC",
    "housenumber": 18,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hendrica",
    "lastname": "Brooks",
    "zipcode": "5651AR",
    "housenumber": 8,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bruna",
    "lastname": "Fernandez",
    "zipcode": "7007LP",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Vere",
    "lastname": "Parker",
    "zipcode": "5283PX",
    "housenumber": 22,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Demi",
    "lastname": "Alexander",
    "zipcode": "5953NE",
    "housenumber": 62,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Conchita",
    "lastname": "Pacheco",
    "zipcode": "2515GB",
    "housenumber": 35,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yvan",
    "lastname": "Schneider",
    "zipcode": "1503MK",
    "housenumber": 226,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Abdirahman",
    "lastname": "Reid",
    "zipcode": "6644BR",
    "housenumber": 18,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nanouk",
    "lastname": "Larson",
    "zipcode": "1391TW",
    "housenumber": 19,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dante",
    "lastname": "Hart",
    "zipcode": "1794HS",
    "housenumber": 98,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Maritza",
    "lastname": "Mccarthy",
    "zipcode": "4273LA",
    "housenumber": 31,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Toke",
    "lastname": "Doyle",
    "zipcode": "2671MP",
    "housenumber": 81,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Giano",
    "lastname": "Wright",
    "zipcode": "1702KD",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Steventje",
    "lastname": "Bradley",
    "zipcode": "1019XX",
    "housenumber": 9,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Peter-Jan",
    "lastname": "Hughes",
    "zipcode": "3818EJ",
    "housenumber": 301,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hielkje",
    "lastname": "Gough",
    "zipcode": "4702GP",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yang",
    "lastname": "Cassidy",
    "zipcode": "1016KD",
    "housenumber": 41,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mickey",
    "lastname": "Carpenter",
    "zipcode": "9601EM",
    "housenumber": 150,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Colette",
    "lastname": "Farmer",
    "zipcode": "1151CW",
    "housenumber": 2,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dafina",
    "lastname": "Leon",
    "zipcode": "3012AS",
    "housenumber": 30,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tanisha",
    "lastname": "Chavez",
    "zipcode": "4241BP",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Zico",
    "lastname": "Wood",
    "zipcode": "5481XV",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mellanie",
    "lastname": "Santiago",
    "zipcode": "2582TA",
    "housenumber": 146,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sumeyra",
    "lastname": "Alexander",
    "zipcode": "9403JS",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Christy",
    "lastname": "Payne",
    "zipcode": "9733BG",
    "housenumber": 103,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Khadya",
    "lastname": "Graham",
    "zipcode": "5583XC",
    "housenumber": 9,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Muhittin",
    "lastname": "Robinson",
    "zipcode": "6551CP",
    "housenumber": 46,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Abbey",
    "lastname": "Rice",
    "zipcode": "1713CZ",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bernadette",
    "lastname": "Li",
    "zipcode": "3253AS",
    "housenumber": 14,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Debra",
    "lastname": "Hamilton",
    "zipcode": "2561XP",
    "housenumber": 23,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bauke",
    "lastname": "Fraser",
    "zipcode": "2011JT",
    "housenumber": 53,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Joy",
    "lastname": "Stewart",
    "zipcode": "2262XT",
    "housenumber": 72,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Maciej",
    "lastname": "Romero",
    "zipcode": "1104CA",
    "housenumber": 4427,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ivor",
    "lastname": "Singh",
    "zipcode": "9711KH",
    "housenumber": 25,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hamid",
    "lastname": "Mack",
    "zipcode": "5041RV",
    "housenumber": 215,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Berra",
    "lastname": "Zimmerman",
    "zipcode": "1991TG",
    "housenumber": 43,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jorike",
    "lastname": "Simmons",
    "zipcode": "6675NB",
    "housenumber": 45,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mahad",
    "lastname": "Hale",
    "zipcode": "1052KV",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Asli",
    "lastname": "Little",
    "zipcode": "2905BP",
    "housenumber": 121,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nimo",
    "lastname": "Mcdaniel",
    "zipcode": "5706XV",
    "housenumber": 10,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Andra",
    "lastname": "Day",
    "zipcode": "4708BD",
    "housenumber": 186,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Su",
    "lastname": "Ramsey",
    "zipcode": "3971BW",
    "housenumber": 6,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Esmeray",
    "lastname": "Garner",
    "zipcode": "4401JS",
    "housenumber": 40,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mila",
    "lastname": "Harrison",
    "zipcode": "2711BN",
    "housenumber": 384,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tigo",
    "lastname": "Fernandez",
    "zipcode": "3299WE",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marelle",
    "lastname": "Strickland",
    "zipcode": "3207TG",
    "housenumber": 21,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Therèse",
    "lastname": "Allison",
    "zipcode": "1705EH",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Salvador",
    "lastname": "Read",
    "zipcode": "9714DS",
    "housenumber": 177,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sera",
    "lastname": "Gilbert",
    "zipcode": "4812NE",
    "housenumber": 37,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Lies",
    "lastname": "Wells",
    "zipcode": "7007LS",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Otte",
    "lastname": "Hodgson",
    "zipcode": "9727JN",
    "housenumber": 34,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Saïd",
    "lastname": "Henry",
    "zipcode": "7815LP",
    "housenumber": 192,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jim",
    "lastname": "Owen",
    "zipcode": "6417CJ",
    "housenumber": 47,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cornelus",
    "lastname": "Maxwell",
    "zipcode": "1053PA",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Hélène",
    "lastname": "Pratt",
    "zipcode": "8072CH",
    "housenumber": 33,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fenneke",
    "lastname": "Norman",
    "zipcode": "5821BV",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gonda",
    "lastname": "Best",
    "zipcode": "9406EA",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sabine",
    "lastname": "Schwartz",
    "zipcode": "5613LL",
    "housenumber": 32,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jalila",
    "lastname": "Crawford",
    "zipcode": "3332EH",
    "housenumber": 19,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Seda",
    "lastname": "Hubbard",
    "zipcode": "6604AH",
    "housenumber": 34,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Suraya",
    "lastname": "Gibbs",
    "zipcode": "1339HP",
    "housenumber": 26,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nezha",
    "lastname": "Wilkes",
    "zipcode": "6991AW",
    "housenumber": 79,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Vanja",
    "lastname": "O'ConnorO'",
    "zipcode": "6035AW",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Romaisae",
    "lastname": "Lindsey",
    "zipcode": "2033CT",
    "housenumber": 18,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Chahira",
    "lastname": "Greene",
    "zipcode": "1971CA",
    "housenumber": 31,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jean",
    "lastname": "Fisher",
    "zipcode": "1051GS",
    "housenumber": 37,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Annelinde",
    "lastname": "English",
    "zipcode": "5212SK",
    "housenumber": 27,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yeşim",
    "lastname": "Serrano",
    "zipcode": "1091HE",
    "housenumber": 84,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tibo",
    "lastname": "Park",
    "zipcode": "5258EB",
    "housenumber": 11,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mohammad",
    "lastname": "Ortega",
    "zipcode": "8921VZ",
    "housenumber": 21,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mient",
    "lastname": "Patrick",
    "zipcode": "9746AT",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Damiën",
    "lastname": "Johnson",
    "zipcode": "4337KD",
    "housenumber": 42,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tabitha",
    "lastname": "Aguilar",
    "zipcode": "5913TZ",
    "housenumber": 54,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tolunay",
    "lastname": "Schultz",
    "zipcode": "5022HV",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Timothëus",
    "lastname": "Curtis",
    "zipcode": "7741AS",
    "housenumber": 4,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jeanet",
    "lastname": "Singleton",
    "zipcode": "5301TB",
    "housenumber": 33,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ahmad",
    "lastname": "Manning",
    "zipcode": "8917HG",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Asma",
    "lastname": "Daniel",
    "zipcode": "2725GX",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nehemia",
    "lastname": "Logan",
    "zipcode": "7064CB",
    "housenumber": 65,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Büsra",
    "lastname": "Woodward",
    "zipcode": "2405EC",
    "housenumber": 71,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fennie",
    "lastname": "Wilkinson",
    "zipcode": "1078EA",
    "housenumber": 189,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sef",
    "lastname": "Walton",
    "zipcode": "7943TJ",
    "housenumber": 11,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Theo",
    "lastname": "Lewis",
    "zipcode": "1054KM",
    "housenumber": 13,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sil",
    "lastname": "Vazquez",
    "zipcode": "1693EA",
    "housenumber": 58,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Robert-Jan",
    "lastname": "Huff",
    "zipcode": "3311TA",
    "housenumber": 285,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Adrie",
    "lastname": "Ramirez",
    "zipcode": "1544PS",
    "housenumber": 5,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alexandre",
    "lastname": "Thomas",
    "zipcode": "9401CK",
    "housenumber": 292,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ouassim",
    "lastname": "Gill",
    "zipcode": "6562LW",
    "housenumber": 30,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Miki",
    "lastname": "Cortez",
    "zipcode": "1394EZ",
    "housenumber": 27,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ridvan",
    "lastname": "Terry",
    "zipcode": "5507LX",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kay",
    "lastname": "Riley",
    "zipcode": "3881XJ",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Abdurrahman",
    "lastname": "Powers",
    "zipcode": "6301JZ",
    "housenumber": 5,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alessia",
    "lastname": "Cole",
    "zipcode": "9725JC",
    "housenumber": 32,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fabiana",
    "lastname": "Long",
    "zipcode": "6562AM",
    "housenumber": 15,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marij",
    "lastname": "Flores",
    "zipcode": "3086HN",
    "housenumber": 247,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bahattin",
    "lastname": "May",
    "zipcode": "5531AX",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mattia",
    "lastname": "Armstrong",
    "zipcode": "5241TL",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alyssia",
    "lastname": "Huang",
    "zipcode": "1022XD",
    "housenumber": 20,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bart-Jan",
    "lastname": "Hopkins",
    "zipcode": "4231DX",
    "housenumber": 12,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alycia",
    "lastname": "Barrett",
    "zipcode": "2546CZ",
    "housenumber": 302,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nine",
    "lastname": "Cannon",
    "zipcode": "1102BP",
    "housenumber": 177,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Keona",
    "lastname": "Rose",
    "zipcode": "6833DB",
    "housenumber": 28,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Waïl",
    "lastname": "Carr",
    "zipcode": "1452PN",
    "housenumber": 53,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Suman",
    "lastname": "Francis",
    "zipcode": "5403PD",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gezinus",
    "lastname": "Howell",
    "zipcode": "8701BG",
    "housenumber": 24,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sjors",
    "lastname": "Wong",
    "zipcode": "1013JR",
    "housenumber": 161,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rodin",
    "lastname": "Pearson",
    "zipcode": "9737SV",
    "housenumber": 212,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rowen",
    "lastname": "Butler",
    "zipcode": "7823AL",
    "housenumber": 495,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Liona",
    "lastname": "Roberson",
    "zipcode": "3533CD",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Phil",
    "lastname": "Mullins",
    "zipcode": "5925NB",
    "housenumber": 219,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jeanice",
    "lastname": "Underwood",
    "zipcode": "2331JG",
    "housenumber": 2,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bodie",
    "lastname": "Phelps",
    "zipcode": "3991LP",
    "housenumber": 46,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Natasja",
    "lastname": "Perkins",
    "zipcode": "7041HG",
    "housenumber": 45,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Alexandra",
    "lastname": "Guerrero",
    "zipcode": "1823GG",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dawid",
    "lastname": "Collins",
    "zipcode": "3705PL",
    "housenumber": 30,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Melissa",
    "lastname": "Allen",
    "zipcode": "3059MK",
    "housenumber": 47,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jelske",
    "lastname": "Calderon",
    "zipcode": "4615HE",
    "housenumber": 34,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Robertus",
    "lastname": "Graves",
    "zipcode": "1412EX",
    "housenumber": 42,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Eleanora",
    "lastname": "Navarro",
    "zipcode": "2312XX",
    "housenumber": 191,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ritske",
    "lastname": "Carrillo",
    "zipcode": "5038SB",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Brett",
    "lastname": "Chavez",
    "zipcode": "5121ND",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rene",
    "lastname": "Hussain",
    "zipcode": "2725ER",
    "housenumber": 64,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Anna-Sophie",
    "lastname": "Aguilar",
    "zipcode": "6681DA",
    "housenumber": 17,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Elyas",
    "lastname": "Thomas",
    "zipcode": "6442BK",
    "housenumber": 24,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Florentien",
    "lastname": "Mccann",
    "zipcode": "5223MD",
    "housenumber": 22,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Joah",
    "lastname": "Knight",
    "zipcode": "4624EX",
    "housenumber": 4,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jozua",
    "lastname": "Garza",
    "zipcode": "3054TD",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Michal",
    "lastname": "Curry",
    "zipcode": "2596SW",
    "housenumber": 51,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Deejay",
    "lastname": "Vaughn",
    "zipcode": "5702HT",
    "housenumber": 35,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kylie",
    "lastname": "Richardson",
    "zipcode": "1091DB",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Mostafa",
    "lastname": "Heath",
    "zipcode": "3862VC",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Harley",
    "lastname": "Burke",
    "zipcode": "1464NK",
    "housenumber": 112,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gus",
    "lastname": "Fleming",
    "zipcode": "7991PH",
    "housenumber": 57,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fatna",
    "lastname": "Powell",
    "zipcode": "6163JV",
    "housenumber": 17,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Brandon",
    "lastname": "Lewis",
    "zipcode": "2282GS",
    "housenumber": 190,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Charisse",
    "lastname": "Hines",
    "zipcode": "9933GT",
    "housenumber": 34,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Miroslav",
    "lastname": "Reyes",
    "zipcode": "2316SK",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ayana",
    "lastname": "Poole",
    "zipcode": "1901JC",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Aradhna",
    "lastname": "Meyer",
    "zipcode": "4484SC",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Adi",
    "lastname": "Dawson",
    "zipcode": "7121VS",
    "housenumber": 59,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fernanda",
    "lastname": "Jordan",
    "zipcode": "5052WG",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Johnson",
    "lastname": "Norman",
    "zipcode": "3453MX",
    "housenumber": 55,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Abdoullah",
    "lastname": "Sherman",
    "zipcode": "2841SW",
    "housenumber": 11,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jacques",
    "lastname": "Patel",
    "zipcode": "6137CG",
    "housenumber": 42,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Florine",
    "lastname": "Mejia",
    "zipcode": "7141DH",
    "housenumber": 6,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Maximilian",
    "lastname": "Gross",
    "zipcode": "1107WN",
    "housenumber": 43,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rosan",
    "lastname": "Richards",
    "zipcode": "2622GB",
    "housenumber": 99,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yaşar",
    "lastname": "Burton",
    "zipcode": "8742KG",
    "housenumber": 5,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Görkem",
    "lastname": "Christensen",
    "zipcode": "9736CN",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Carlin",
    "lastname": "Mills",
    "zipcode": "2729KV",
    "housenumber": 127,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Ladan",
    "lastname": "Malone",
    "zipcode": "3445BA",
    "housenumber": 7,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Yousef",
    "lastname": "Graham",
    "zipcode": "7384BK",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Andy",
    "lastname": "Luna",
    "zipcode": "6141BK",
    "housenumber": 30,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jovana",
    "lastname": "King",
    "zipcode": "7275AE",
    "housenumber": 7,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Randi",
    "lastname": "Naylor",
    "zipcode": "1502BL",
    "housenumber": 136,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nergiz",
    "lastname": "Casey",
    "zipcode": "4811PE",
    "housenumber": 55,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Danice",
    "lastname": "Taylor",
    "zipcode": "4851TK",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Deniece",
    "lastname": "Tucker",
    "zipcode": "1962BG",
    "housenumber": 138,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Cedric",
    "lastname": "Jenkins",
    "zipcode": "2987SZ",
    "housenumber": 631,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Stoffel",
    "lastname": "Johnson",
    "zipcode": "9051EA",
    "housenumber": 41,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fredrika",
    "lastname": "Berry",
    "zipcode": "7327EV",
    "housenumber": 203,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Aleksander",
    "lastname": "Gutierrez",
    "zipcode": "4521AP",
    "housenumber": 23,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nanko",
    "lastname": "Buchanan",
    "zipcode": "5914PE",
    "housenumber": 25,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sona",
    "lastname": "Gibson",
    "zipcode": "5912HR",
    "housenumber": 46,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Germaine",
    "lastname": "Carpenter",
    "zipcode": "1474HT",
    "housenumber": 93,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Carsten",
    "lastname": "Fields",
    "zipcode": "1676GR",
    "housenumber": 15,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jeffrey",
    "lastname": "Stewart",
    "zipcode": "9281NA",
    "housenumber": 30,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Jair",
    "lastname": "Lord",
    "zipcode": "1321VA",
    "housenumber": 43,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Boi",
    "lastname": "Willis",
    "zipcode": "6096AX",
    "housenumber": 30,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Vincentius",
    "lastname": "Dale",
    "zipcode": "1017SC",
    "housenumber": 125,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Brayn",
    "lastname": "Barton",
    "zipcode": "5216XL",
    "housenumber": 54,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Eugène",
    "lastname": "Castaneda",
    "zipcode": "2584SN",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kai",
    "lastname": "Daniel",
    "zipcode": "2265DV",
    "housenumber": 33,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Shayan",
    "lastname": "Wise",
    "zipcode": "6093GM",
    "housenumber": 24,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Michał",
    "lastname": "Bell",
    "zipcode": "7101NB",
    "housenumber": 63,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Manouck",
    "lastname": "French",
    "zipcode": "2161ML",
    "housenumber": 26,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Kirsty",
    "lastname": "Stephens",
    "zipcode": "1087LM",
    "housenumber": 312,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Davie",
    "lastname": "Wyatt",
    "zipcode": "2935AW",
    "housenumber": 28,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Quincey",
    "lastname": "Byrne",
    "zipcode": "6709RL",
    "housenumber": 207,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gilian",
    "lastname": "Austin",
    "zipcode": "3201EM",
    "housenumber": 45,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Xu",
    "lastname": "Ward",
    "zipcode": "6581XT",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Apolonia",
    "lastname": "Mac",
    "zipcode": "4793BJ",
    "housenumber": 16,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Issam",
    "lastname": "Wood",
    "zipcode": "3271VE",
    "housenumber": 33,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Joaquin",
    "lastname": "Burns",
    "zipcode": "6605ZK",
    "housenumber": 3540,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gerke",
    "lastname": "Schwartz",
    "zipcode": "3601GR",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dianne",
    "lastname": "Carr",
    "zipcode": "2805KB",
    "housenumber": 114,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Demelsa",
    "lastname": "Moody",
    "zipcode": "6221BG",
    "housenumber": 57,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Irfaan",
    "lastname": "Montgomery",
    "zipcode": "7251CL",
    "housenumber": 45,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Syl",
    "lastname": "Barker",
    "zipcode": "2965CG",
    "housenumber": 145,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marjolijne",
    "lastname": "Schultz",
    "zipcode": "9231HE",
    "housenumber": 23,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Roeland",
    "lastname": "Green",
    "zipcode": "2221RG",
    "housenumber": 78,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Anneroos",
    "lastname": "Todd",
    "zipcode": "8307DA",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Monir",
    "lastname": "Brooks",
    "zipcode": "9079KG",
    "housenumber": 1,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Emie",
    "lastname": "Fernandez",
    "zipcode": "1831CN",
    "housenumber": 161,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rainier",
    "lastname": "Parker",
    "zipcode": "5672AJ",
    "housenumber": 52,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Robbert",
    "lastname": "Alexander",
    "zipcode": "4158EC",
    "housenumber": 54,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wilma",
    "lastname": "Pacheco",
    "zipcode": "2628LW",
    "housenumber": 202,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Imelda",
    "lastname": "Schneider",
    "zipcode": "6531MD",
    "housenumber": 151,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Bernadina",
    "lastname": "Reid",
    "zipcode": "2231PB",
    "housenumber": 1,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Gabriëlle",
    "lastname": "Larson",
    "zipcode": "9354TL",
    "housenumber": 3,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Sheila",
    "lastname": "Hart",
    "zipcode": "6442AA",
    "housenumber": 49,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Pakize",
    "lastname": "Mccarthy",
    "zipcode": "5038TK",
    "housenumber": 71,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Annebel",
    "lastname": "Doyle",
    "zipcode": "6713JD",
    "housenumber": 10,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Naoufal",
    "lastname": "Wright",
    "zipcode": "6717EA",
    "housenumber": 103,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Otto",
    "lastname": "Bradley",
    "zipcode": "3711AR",
    "housenumber": 55,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fien",
    "lastname": "Hughes",
    "zipcode": "8447DT",
    "housenumber": 9,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Adeline",
    "lastname": "Gough",
    "zipcode": "8431SG",
    "housenumber": 19,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marèll",
    "lastname": "Cassidy",
    "zipcode": "9411HH",
    "housenumber": 32,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fardin",
    "lastname": "Carpenter",
    "zipcode": "3454RC",
    "housenumber": 95,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wilfred",
    "lastname": "Farmer",
    "zipcode": "7827PH",
    "housenumber": 41,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Nadin",
    "lastname": "Leon",
    "zipcode": "1781JK",
    "housenumber": 20,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Seçil",
    "lastname": "Chavez",
    "zipcode": "2353SW",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Angenita",
    "lastname": "Wood",
    "zipcode": "2681RA",
    "housenumber": 12,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dursun",
    "lastname": "Santiago",
    "zipcode": "3412KG",
    "housenumber": 152,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Haico",
    "lastname": "Alexander",
    "zipcode": "8801VM",
    "housenumber": 12,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Dillon",
    "lastname": "Payne",
    "zipcode": "1871HB",
    "housenumber": 3,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Geesje",
    "lastname": "Graham",
    "zipcode": "4793EG",
    "housenumber": 32,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Symon",
    "lastname": "Robinson",
    "zipcode": "9204WC",
    "housenumber": 2,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Richtje",
    "lastname": "Rice",
    "zipcode": "1567LK",
    "housenumber": 27,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Darlene",
    "lastname": "Li",
    "zipcode": "2991KM",
    "housenumber": 96,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Fabiènne",
    "lastname": "Hamilton",
    "zipcode": "2333VJ",
    "housenumber": 10,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tanita",
    "lastname": "Fraser",
    "zipcode": "7702AH",
    "housenumber": 23,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tseard",
    "lastname": "Stewart",
    "zipcode": "5932SE",
    "housenumber": 70,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Tren",
    "lastname": "Romero",
    "zipcode": "4941VD",
    "housenumber": 109,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Wouterus",
    "lastname": "Singh",
    "zipcode": "8918BV",
    "housenumber": 596,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Marco",
    "lastname": "Mack",
    "zipcode": "2286JC",
    "housenumber": 17,
    "parceltype": "LARGE",
    "deliverydate": "20-07-2023"
  },
  {
    "firstname": "Rene",
    "lastname": "Zimmerman",
    "zipcode": "2911CG",
    "housenumber": 6,
    "parceltype": "SMALL",
    "deliverydate": "20-07-2023"
  }
]);